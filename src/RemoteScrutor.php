<?php

    namespace Loggeur;

    use Dotenv\Dotenv;

    class RemoteScrutor{

        //URL de l'api, définie en dur par défaut
        private $remoteURI = "";

        //Nom du script et du projet
        private $scriptName = "";
        private $projectName = "";

        //Variable pour stocker le token
        private $apiToken = "";

        //Les logins en dur par défaut
        private $apiUser = "";
        private $apiPassword = "";

        //Variable d'état en cas d'erreur
        private $_errorReport = 0;
        private $apiFailed = false;
        private $curlErrorMessage = "";

        //Mode 1 -> Tout sur l'api et les erreurs sur slack;
        //Mode 2 -> Tout sur slack;
        private $reportMode = 1;
        private $stopSended = false;

        //slack
        private $channelSlack = "test";
        private $tokenSlack = "";
        private $usernameSlack = "Bruno le robot rigolo";
        private $slackFailed = false;
        private $curlErrorMessageSlack = "";

        //Old error handler
        private $oldErrorHandler;
        private $oldExceptionHandler;

        /**
         * Constructeur qui prend le nom du script et le nom du projet en paramètres et qui initialise directement la connexion
         */
        function __construct($_scriptName, $_projectName, $dir){
            $this->oldErrorHandler = set_error_handler(array($this, "globalErrorHandler"));
            $this->oldExceptionHandler = set_exception_handler(array($this, "globalExceptionHandler"));
            register_shutdown_function(array($this, "globalShutdownCallback"));

            $dotenv = \Dotenv\Dotenv::create($dir);
            $dotenv->load();

            $this->tokenSlack = getenv('SLACK_TOKEN');
            $this->apiUser = getenv('API_USER');
            $this->apiPassword = getenv('API_PASSWORD');
            $this->remoteURI = getenv('API_URL');

            if(getenv('SLACK_CHANNEL') != ''){
                $this->channelSlack = getenv('SLACK_CHANNEL');
            }

            if(getenv('SLACK_USERNAME') != ''){
                $this->usernameSlack = getenv('SLACK_USERNAME');
            }

            $this->scriptName = $_scriptName;
            $this->projectName = $_projectName;
            $this->connectApi();
        }

        /**
         * Fonction dédiée a la connection avec l'api
         */
        private function connectApi(){
            //Envois des paramètre stocker dans la classe
            $data = array(
                "email" => $this->apiUser, 
                "password" => $this->apiPassword
            );
            
            $bundle = $this->_apiSend($data, "/auth/sign_in");

            $info = $bundle["info"];
            $result = $bundle["result"];

            //Si une erreur http a été levée, on réagis et on switch
            if($info["http_code"] == 200){
                //Si tout ce passe bien, on récupére le token pour les futurs appels
                $this->apiToken = json_decode($result, true)["token"];
            }
        }

        /**
         * Fonction généraliste qui sera appeler a chaque appel a l'api et qui gère les erreurs pour switcher de mode
         */
        private function _apiSend($data, $path, $auth = false){
            //Les headers par défaut
            $headers = array();
            $headers[] = "Content-Type:application/json";
            //Si le token est nécéssaire (par défaut non)
            if($auth)
                $headers[] = "Authorization: Bearer ".$this->apiToken;

            //On encode les données en json
            $data_string = json_encode($data);
            
            $curl = curl_init();

            //Définis toutes les options curl, dont le timeout pour éviter que le script soit ralentis plus de 5sec
            //Apres ce temps, on considère l'api comme non accesible et on switch
            curl_setopt($curl, CURLOPT_URL, $this->remoteURI.$path);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_TIMEOUT_MS, 5000);

            //Execution et récupération des éventuelle erreurs
            $result = curl_exec($curl);
            $info = curl_getinfo($curl);
            $errno = curl_errno($curl);

            if($errno > 0) {
                $error_message = curl_strerror($errno);
                echo "cURL error ({$errno}):\n {$error_message}"."\n";
                echo "Problem: ".$error_message." in apiSend\n";
                $this->curlErrorMessage = $error_message;
                //Si curl a levé une erreur, on switch de mode
                $this->apiFailed = true;
                $this->switchMode(2);
            }else{
                //Si une erreur http a été levée, on réagis et on switch
                if($info["http_code"] == 401){
                    $this->apiFailed = true;
                    $this->switchMode(2);
                    if($this->curlErrorMessage == "")
                        $this->curlErrorMessage = "Authentication failed, error 401 received.";
                }elseif($info["http_code"] != 200){
                    //Erreur non gérée, comportement a définir
                    echo "Problem: ".$info["http_code"]." in apiSend\n";
                    //On switch de mode
                    $this->switchMode(2);
                    if($this->curlErrorMessage == "")
                        $this->curlErrorMessage = "Send API failed, error ".$info["http_code"]." received.";
                }
            }

            //Sinon on renvois les infos
            return array(
                "info" => $info,
                "result" => $result
            );
        }

        /**
         * Fonction qui envois un message générique de début de script
         */
        public function start(){
            $now = new \DateTime();
            //On précises toutes les options nécésaire, dont le type "start" et la date
            $data = array(
                "processName" => $this->scriptName,
                "projectName" => $this->projectName,
                "logType" => "start",
                "throwDate" => $now->format("Y-m-d H:i:s.v"),
                "details" => "Le script a démarrer.",
                "error" => $this->_errorReport
            );

            if($this->reportMode == 1){
                $bundle = $this->_apiSend($data, "/logReport", $auth = true);
            }elseif($this->reportMode == 2){
                $this->_slackSend($this->scriptName." || Début du script.");
            }
        }

        /**
         * Fonction qui envois un message générique de fin de script
         */
        public function stop(){
            
            if(!$this->stopSended){
                $this->stopSended = true;
                $now = new \DateTime();
                //On précises toutes les options nécésaire, dont le type "stop" et la date
                $data = array(
                    "processName" => $this->scriptName,
                    "projectName" => $this->projectName,
                    "logType" => "stop",
                    "throwDate" => $now->format("Y-m-d H:i:s.v"),
                    "details" => "Le script est terminé.",
                    "error" => $this->_errorReport
                );
    
                if($this->reportMode == 1){
                    $bundle = $this->_apiSend($data, "/logReport", $auth = true);
                }elseif($this->reportMode == 2){
                    $this->_slackSend($this->scriptName." || Fin du script.");
                }
            }
        }

        /**
         * Fonction qui permet d'envoyer des logReports de 'running', lorsque le script tourne, cela peut être des données de suivies(nombre de personnes traités, etc..)
         * Le paramètre error permet de préciser si c'est une erreur, l'api pourra alors réagir d'une manière précise
         */
        public function report($text, $error = 0){
            $now = new \DateTime();
            //On précises toutes les options nécésaire, dont le type "run" et la date
            $data = array(
                "processName" => $this->scriptName,
                "projectName" => $this->projectName,
                "logType" => "run",
                "throwDate" => $now->format("Y-m-d H:i:s.v"),
                "details" => $text,
                "error" => $error
            );

            $this->_errorReport = $error;

            if($this->reportMode == 1){
                $bundle = $this->_apiSend($data, "/logReport", $auth = true);
            }elseif($this->reportMode == 2){
                $this->_slackSend($this->scriptName." || ".$text);
            }
        }

        
        private function _slackSend($message){
            $curl = curl_init();
            $headers = array();

            //Renseignement des options curl
            curl_setopt($curl, CURLOPT_URL, "https://slack.com/api/chat.postMessage");

            //Initialisation des headers
            $headers[] = "Content-Type:application/json";
            $headers[] = "Authorization: Bearer ".$this->tokenSlack;
            
            //Partie message
            $data =array(
                //Le lien vers l'image qui sera afficher
                "icon_url" => "https://image.noelshack.com/fichiers/2019/19/5/1557501247-zoidberg2.png",
                //Le nom qui sera utilisé par le robot
                "username" => $this->usernameSlack,
                "link_names" => true,
                //Le channel a utilisé
                "channel" => $this->channelSlack,
                //L'attachment correspond au message en lui-même
                "attachments" => json_encode(array(array(
                    //La couleur rouge ou verte si le script a un problem ou non
                    "color" => $this->_errorReport == 0 ? "good" : "danger",
                    //Nom du projet
                    "title" => $this->projectName,
                    //Lien sur le nom du projet 
                    "title_link" => "https://vps.edt.ovh/api-docs/",
                    //Text du message
                    "text" => $message,
                    //Le footer avec le nom et la même image que l'icon
                    "footer" => "Scrutor API",
                    "footer_icon" => "https://image.noelshack.com/fichiers/2019/19/5/1557501247-zoidberg2.png",
                )))
            );
            
            $data_string = json_encode($data);

            //Autres options curl
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
            //Fonction pour que curl n'echo rien
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($curl);

            $info = curl_getinfo($curl);
            $errno = curl_errno($curl);

            if($errno > 0) {
                //Erreur brut de curl
                $error_message = curl_strerror($errno);
                echo "cURL error ({$errno}):\n {$error_message}"."\n";
                //Si curl a levé une erreur, on switch de mode
                $this->slackFailed = true;
                $this->curlErrorMessageSlack = $error_message;
            }

            //Si une erreur http a été levée, on réagis et on switch
            if($info["http_code"] != 200){
                //Erreur non gérée, comportement a définir
                echo "Problem: ".$info["http_code"]." in slackSend\n";
                $this->slackFailed = true;
                if($this->curlErrorMessageSlack == "")
                    $this->curlErrorMessageSlack = "Slack message send failed, received error: ".$info["http_code"];
            }
        }

        public function switchMode($_reportMode){
            $this->reportMode = $_reportMode;
            if($this->reportMode == 2 ){
                if($this->apiFailed = true){
                    $this->_slackSend($this->scriptName." || "."RemoteScrutor à rencontré un problème et à du switcher de mode de log.\nErreur rencontrée: ".$this->curlErrorMessage);
                }else{
                    $this->_slackSend($this->scriptName." || "."RemoteScrutor à changé de mode de log volontairement.");
                }
            }
        }

        public function globalErrorHandler($errno, $errstr, $errfile, $errline){
            $errno_trad = "";
            $error_level = -1;
            switch ($errno) {
                case E_USER_ERROR:
                    $errno_trad = "ERROR";
                    $error_level = 4;
                    break;
            
                case E_USER_WARNING:
                    $errno_trad = "WARNING";
                    $error_level = 3;
                    break;
            
                case E_USER_NOTICE:
                    $errno_trad = "NOTICE";
                    $error_level = 2;
                    break;
            
                default:
                    $errno_trad = "OTHER";
                    $error_level = 1;
                    break;
            }

            $texte = "Une erreur à été capturée\nErreur: ".$errno_trad."(".$errno.")\nDétails erreur: ".$errstr."\nLigne de l'erreur: ".$errline."\nFichier source: ".$errfile;
            $this->report($texte, $error = $error_level);

            return true;
        }

        public function globalExceptionHandler($exception){
            $texte = "Une exception a été capturée\nMessage exception: ".$exception->getMessage()."\nLigne exception: ".$exception->getLine()."\nFichier source: ".$exception->getFile()."\nTrace exception: ".$exception->getTrace();
            $this->report($texte, $error = 1);
        }

        public function globalShutdownCallback(){
            $error = error_get_last();

            if($error !== NULL){
                $errno   = $error["type"];
                $errfile = $error["file"];
                $errline = $error["line"];
                $errstr  = $error["message"];

                $texte = "Une erreur à été capturée a la fin d'un script\nErreur: (".$errno.")\nDétails erreur: ".$errstr."\nLigne de l'erreur: ".$errline."\nFichier source: ".$errfile;
                $this->report($texte, $error = 4);
            }

            $this->stop();
        }
    }
?>

## Pourquoi ?
C'est un SDK PHP pour simplifier l'utilisation de l'API Scrutor avec des fonctions pré-définies.

## Fonctionnalitées:
### Fonctions:
#### start():
Prévenir du lancement du script
#### stop(): 
Prévenir de la fin du script
#### report(string $val, int(0 -> 4): $errorLevel): 
Rapporter un état, une information importante et possiblement une erreur avec un niveau de 1 a 4
#### switchMode(int(1 -> 2)): 
Changer le mode de rapport, 1 -> API, 2 -> Slack

### Autres:
#### Error handler personnalisé:
Le SDK redéfinie le catcher d'erreurs pour attraper les erreurs E_USER_*, il n'est possible d'attraper les erreurs (E_ERROR, E_PARSE, E_CORE_ERROR, E_CORE_WARNING, E_COMPILE_ERROR, E_COMPILE_WARNING, E_STRICT), mais permet de fonctionner dans le script avec des trigger d'erreurs. Le handler enverra un report() a l'API avec un niveau d'erreurs en fonction de la gravité de l'erreur.

#### Exception handler personnalisé:
De la même manière, le SDK redéfini le catcher d'exception, si une exception n'est pas attrapée dans un bloc try/catch, le catcher s'en occupe, envoi un rapport et l'exécution du script est stopper juste après.

#### Shutdown callback personnalisé: 
Le SDK défini une fonction de callback qui sera exécuter lorsque le script prendra fin, que ce soit à cause d'une erreur ou non, dans ce callback il vérifie la cause de la fin( "naturelle" ou à cause d'une erreur), et enverra un rapport sur cette erreur si nécessaire, ensuite il exécutera la fonction stop() si le script ne l'avait pas encore fait.

## Sécurité
Pour être certains que l'information ne soit pas perdue, une sécurité a était mise place, si l'API ne répond pas(serveur down, ...) ou qu'elle est trop longue à répondre sur une des requête(5 sec), le Loggeur change de mode de rapport et envois toutes les informations directement dans Slack.

Dans ce mode, toutes les informations bug ou non sont envoyée directement envoyer dans Slack.

Sinon, l'API décide selon des règles, si l'information doit être remontée ou non(pour l'instant, si le niveau d'erreur est supérieur à 1)

## Installation

Le SDK est disponible sur composer :
https://packagist.org/packages/martin-tw/remote-scrutor

La v1.0.0 utilise un package composer pour importer les paramètre depuis un .env et nécessite donc Composer:
>composer require martin-tw/remote-scrutor:v1

Cependant une version standalone est disponible sans aucune dépendance :
>composer require martin-tw/remote-scrutor:standalone

>wget https://bitbucket.org/duboismartin/scrutorsdk-php/raw/c8ec7cbc09f6f768c3e6ba375d74e231838fea02/RemoteScrutor.php

Une fois installer, il faut importer le Loggeur:

	require_once("RemoteScrutor.php"); // Pour la version standalone

	require_once(__DIR__ . '/vendor/autoload.php'); // Pour la version avec gestion du .env
	use Loggeur\RemoteScrutor;

Pour configurer le Loggeur:

Pour la version standalone
    $test = new RemoteScrutor(
        array(
            "scriptName" => "_scriptName", 
            "projectName" => "_projectName", 
            "tokenSlack" => "_tokenSlack", 
            "apiUser" => "_apiUser", 
            "apiPassword" => "_apiPassword", 
            "remoteUri" => "_remoteUri", 
            "channelSlack" => "", // Si vide = test
            "usernameSlack" => "" // Si vide = Bruno le robot rigolo
        )
    );

Pour la version avec la gestion du .env
    $test = new RemoteScrutor("_projectName", "_processName", __DIR__); // _DIR__ est le répertoire ou est situé le .env
Un .env.example est disponible sur le dépot git.